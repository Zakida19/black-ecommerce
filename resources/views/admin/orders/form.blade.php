@extends('layouts.master-admin')

@section('content')
<div class="row">
    <!-- Basic Layout -->
    <div class="col-xxl">
      <div class="card mb-4">
        <div class="card-header d-flex align-items-center justify-content-between">
          <h5 class="mb-0">Edit Order</h5>
          <small class="text-muted float-end">Default label</small>
        </div>
        <div class="card-body">
          <form action="post" method="{{ route('order.update', $order) }}" id="submitForm">
            @csrf
            @method('put')
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label" for="basic-default-name">Status Pesanan</label>
              <div class="col-sm-10">
                <input class="form-check-input" name="shipped" type="checkbox" value="1" {{ $order->shipped == 1 ? 'checked': '' }}>
                <label class="form-check-label">Dikirim</label>
              </div>
            </div>
            <div class="row justify-content-end">
              <div class="col-sm-10">
                <button type="submit" submitForm="submit" class="btn btn-primary">Update</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection
