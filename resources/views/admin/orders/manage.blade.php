@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('layouts.message')
        <div class="card">
          <div class="card-header">
            <div class="card-title">
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table table-striped" id="dataOrder">
              <thead>
                <tr>
                    @foreach($table['columns'] as $column)
                    <th>{{ $column['label'] }}</th>
                    @endforeach
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#dataOrder').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ $table['table_url'] }}',
        ordering: false,
        searching: false,
        paging: false,
        autoWidth: false,
        lengthChange: false,
        info: false,
        columns: [
            @foreach($table['columns'] as $column)
            {data: '{{ $column['name'] }}', name: '{{ $column['name'] }}'},
            @endforeach
        ],
    });
});
</script>
@endpush
