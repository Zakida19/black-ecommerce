@extends('layouts.master-admin')

@section('content')
@php
if (isset($product)) {
    $actionUrl = route('product.update', $product->id);
} else {
    $actionUrl = route('product.store');
}
@endphp
<div class="row">
    <div class="col-md-12">
    <div class="card card-seccond">
        <div class="card-header">
        <h3 class="card-title">Update Produk</h3>
        </div>
        <form id="submitProduct" method="POST" action="{{ $actionUrl }}" enctype="multipart/form-data">
            @if (@isset($product))
                {{ method_field('PUT') }}
                <input type="hidden" name="user_id" value="{{ $product->id }}" />
            @endif
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control" name="category_id" id="category_id" style="width: 100%;">
                        <option value="">Pilih kategori</option>
                    @foreach($categoryList as $category)
                    <option {{ isset($product) && $product->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                    </select>
                    @error('category')
                        <div class="mt-2 text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="name">Nama Produk</label>
                    <input type="text" class="form-control" name="name_product" id="name_product" placeholder="Nama Kategori" value="{{ isset($product) ? $product->name_product : old('name_product') }}">

                    @error('name_product')
                        <div class="mt-2 text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Product Image</label>
                    @if (isset($product))
                        <div class="mb-3">
                            <img src="{{ asset('/images/products/'. $product->image_product) }}" alt="" style="width: 170px;" height="120px" class="img-rounded">
                        </div>
                    @endif
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="image_product" id="image_product">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>
                    </div>

                    @error('image_product')
                        <div class="mt-2 text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="name">Deskripsi</label>
                    <textarea class="form-control" rows="3" name="description" id="description" placeholder="Deskripsi ...">{{ isset($product) ? $product->description : old('description') }}</textarea>

                    @error('description')
                        <div class="mt-2 text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="name">Price</label>
                    <input type="number" class="form-control" name="price" id="price" placeholder="Price" value="{{ isset($product) ? $product->price : old('price') }}">
                    @error('price')
                        <div class="mt-2 text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="name">Quantity Item</label>
                    <input type="number" class="form-control" name="quantity" id="quantity" placeholder="Quantity Item" value="{{ isset($product) ? $product->quantity : old('quantity') }}">
                    @error('quantity')
                        <div class="mt-2 text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" submitProduct="submit" class="btn btn-info btn-sm">Submit</button>
            </div>
        </form>
    </div>
    </div>
</div>
@endsection
