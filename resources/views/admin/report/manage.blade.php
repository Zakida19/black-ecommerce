@extends('layouts.master-admin')

@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h5 class="card-header">Manage Kategori</h5>
        <a href="btn btn-primary">
            <span class="tf-icons bx bx-plus"></span>&nbsp; Create
        </a>
    </div>
    <div class="card-body">
        <div class="table-responsive text-nowrap">
        <table class="table">
            <thead>
            <tr>
                <th>Kategori</th>
                <th>Deskripsi</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody class="table-border-bottom-0">
            <tr>
                <td><i class="fab fa-angular fa-lg text-danger me-3">
                    </i> <strong>Angular Project</strong>
                </td>
                <td>Albert Cook</td>
                <td>
                <div class="dropdown">
                    <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                    <i class="bx bx-dots-vertical-rounded"></i>
                    </button>
                    <div class="dropdown-menu">
                    <a class="dropdown-item" href="javascript:void(0);"
                        ><i class="bx bx-edit-alt me-1"></i> Edit</a
                    >
                    <a class="dropdown-item" href="javascript:void(0);"
                        ><i class="bx bx-trash me-1"></i> Delete</a
                    >
                    </div>
                </div>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
