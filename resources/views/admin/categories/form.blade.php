@extends('layouts.master-admin')

@section('content')
@php
    if (isset($category)) {
        $actionUrl = route('category.update', $category->id);
    } else {
        $actionUrl = route('category.create');
    }
@endphp
<div class="row">
    <div class="col-md-12">
      <div class="card card-seccond">
        <div class="card-header">
          <h3 class="card-title">Edit Kategori</h3>
        </div>
        <form id="updateCategory" method="post" action="{{ $actionUrl }}" enctype="multipart/form-data">
            @if (@isset($category))
                {{ method_field('put') }}
                <input type="hidden" name="user_id" value="{{ $category->id }}" />
            @endif
        @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="name">Nama Kategori</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Nama Kategori" value="{{ isset($category) ? $category->name : old('name') }}">
              @error('name')
                <div class="mt-2 text-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="name">Deskripsi</label>
                <textarea class="form-control" rows="3" name="description" id="description" placeholder="Deskripsi ...">{{ isset($category) ? $category->description : old('description') }}</textarea>
                @error('description')
                  <div class="mt-2 text-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-info btn-sm">Simpan</button>
          </div>
        </form>
      </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#submitCategory').submit(function(e) {
        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');
        var method = form.attr('method');
        var dataCategory = form.serialize();

        $.ajax({
            url: url,
            type: method,
            data: dataCategory,
            datType: 'json',
            success: function(response) {
                window.location = '/category/manage'
                console.log(response);
                // Display success message or perform any other actions
            },
            error: function(xhr) {
                console.log(xhr.responseText);
                // Handle error response
            }
        });
    });
});
</script>
@endpush
