@extends('layouts.master-admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        @include('layouts.message')
        <div class="card">
          <div class="card-header">
            <div class="card-title">
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </div>
                </div>
            </div>

            <div class="card-tools">
                <span class="badge">
                    @if (@isset($table['create']))
                    <a href="{{ $table['create']['url'] }}" type="button" class="btn btn-info btn-sm">{{ $table['create']['label'] }}</a>
                    @endif
                </span>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table table-striped"  id="dataCategory">
                <thead>
                    <tr>
                        @foreach($table['columns'] as $column)
                        <th>{{ $column['label'] }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
          </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#dataCategory').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ $table['table_url'] }}',
        ordering: false,
        searching: false,
        paging: false,
        autoWidth: false,
        lengthChange: false,
        info: false,
        columns: [
                @foreach($table['columns'] as $column)
                { data: '{{ $column['name'] }}', name: '{{ $column['name'] }}' },
            @endforeach
        ],

        // Delete da from datatable
        drawCallback: function(){
            // Delete Confirmation
            $(".delete-form").on("click", function(){
                var form = $(this).parent().find("form");
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Apakah kamu yakin ingin menghapus kategori ini?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                }).then((result) => {
                    if (result.value) {
                        form.submit();
                    }
                })
            });
        }
    });

    // $('.delete-form').on('submit', function(e) {
    //     e.preventDefault();

    //     $.ajax({
    //         type: 'post',
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         url: $(this).data('route'),
    //         data: {
    //         '_method': 'delete'
    //         },
    //         success: function (response, textStatus, xhr) {
    //         // alert('Data yang anda pilih akan dihapus')
    //         window.location='/category/manage'
    //         }
    //     });
    // })
});
</script>
@endpush
