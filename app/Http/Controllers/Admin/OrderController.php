<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function manage()
    {
        $data['table'] = [
            'table_url' => route('order.data'),
            'columns' => [
                [
                    'name' => 'user_name',
                    'label' => 'Pelanggan'
                ],
                [
                    'name' => 'order_date',
                    'label' => 'Tanggal Pesanan'
                ],
                [
                    'name' => 'total_amount',
                    'label' => 'Total Jumlah',
                ],
                [
                    'name' => 'status_order',
                    'label' => 'Status Pesanan',
                ],
                [
                    'name' => 'action',
                    'label' => 'Manage',
                ],
            ]
        ];
        return view('admin.orders.manage', $data);
    }

    public function getData()
    {
        try {
            $query = Order::select([
                'orders.id',
                'order_date',
                'total_amount',
                'status_order',
                'users.name as user_name'
            ])
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->orderBy('order_date', 'desc');

            return DataTables::of($query)
            ->addColumn('action', function($order) {
                    $string = '<a href="'.route('order.edit', $order->id).'" class="btn btn-info btn-sm mr-3 " style="margin-right: 10px;"><i class="fas fa-edit"></i></a>';

                    $string .= '<button title="Hapus" class="btn btn-icon btn-sm btn-danger waves-effect waves-light delete-form"><i class="fa fa-trash"></i></button>';

                    $string .= '<form action="' . route('order.delete', $order->id) . '" method="POST">' . method_field('delete') . csrf_field() . '</form>';

                    return $string;
            })
            ->rawColumns(['action'])
            ->make(true);
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    public function orderDetail($id)
    {
        $detail['order'] = Order::findOrFail($id);
        try {
            if ($detail) {
                return view('admin.orders.detail', $detail);
            }
        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function editStatus(Order $order)
    {
        $data['order'] = $order;
        return view('admin.orders.form', $data);
    }

    public function shipped(Request $request, Order $order)
    {
            $input['shipped'] = $request->input('shipped') ? true : false;
        try {
            $update =  $order->update($input);

            if ($update) {
                return redirect()->route('order.manage')->with('success', 'Berhasil mengubah status pesanan');
            }
            return redirect()->back()->with('error', 'Gagal mengubah status pesanan');
        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function delete(String $id)
    {
        $delete = Order::findOrfail($id)->delete();
        try {
            if ($delete) {
                return redirect()->route('order.manage')->with('success', 'Berhasil menghapus data order');
            }
            return redirect()->back()->with('success', 'Gagal menghapus data order');

        } catch (\Exception $error) {
            return redirect()->back()->with('success', $error->getMessage());
        }
    }
}
