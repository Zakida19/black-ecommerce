<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    public function manage()
    {
        $data['table'] = [
            'table_url' => route("category.data"),
            'create' => [
                'url' => route('category.create'),
                'label' => 'Tambah Kategori'
            ],
            'columns' => [
                [
                    'name' => 'name',
                    'label' => 'Nama Kategori'
                ],
                [
                    'name' => 'description',
                    'label' => 'Deskripsi'
                ],
                [
                    'name' => 'created_at',
                    'label' => 'Tanggal Dibuat',
                ],
                [
                    'name' => 'action',
                    'label' => 'Manage',
                ],
            ]
        ];
        return view('admin.categories.manage', $data);
    }

    public function getData()
    {
        try {
            $query = Category::select([
                'categories.id',
                'name',
                'description',
                'created_at',
            ])->orderBy('name', 'asc');

            return DataTables::of($query)
            ->addColumn('created_at', function($item) {
                return date('d-m-Y H:is', strtotime($item->created_at));
            })
            ->addColumn('action', function($category) {
                $string = '<a href="'.route('category.edit', $category->id).'" class="btn btn-info btn-sm mr-3 " style="margin-right: 10px;"><i class="fas fa-edit"></i></a>';

                $string .= '<button title="Hapus" class="btn btn-icon btn-sm btn-danger waves-effect waves-light delete-form"><i class="fa fa-trash"></i></button>';

                $string .= '<form action="' . route('category.delete', $category->id) . '" method="POST">' . method_field('delete') . csrf_field() . '</form>';

                return $string;
            })
            ->rawColumns(['action'])
            ->make(true);

        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    public function create()
    {
        return view('admin.categories.form');
    }

    public function store(CategoryRequest $request)
    {

        $input = $request->all();
        $input['slug'] = Str::slug($request->input('name'));

        try {
            $create = Category::create($input);
            if ($create) {
                return redirect()->route('category.manage')->with('success', 'Berhasil menambahkan kategoori');
            }

            return redirect()->back()->with('error', 'Gagal menambahkan kategori');

        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function edit(Category $category)
    {
        try {
            $data['category'] = $category;
            return view('admin.categories.form', $data);
        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $inputUpdate = $request->all();
        $inputUpdate['slug'] = Str::slug($request->input('name'));

        try {
            $update = $category->update($inputUpdate);

            if ($update) {
                return redirect()->route('category.manage')->with('succes', 'Berhasil mengubah kategori');
            }
                return redirect()->back()->with('error', 'Gagal mengubah data kategori');
        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function delete(String $id)
    {
        $category = Category::findOrFail($id)->delete();
        try {
            if ($category) {
                return redirect()->route('category.manage')->with('success', 'Berhasil menghapus kategori');
            }
            return redirect()->back()->with('error', 'Gagal menghapus kategori');
        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }

    }
}
