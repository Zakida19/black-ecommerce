<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    public function manage()
    {
        $data['table'] = [
            'table_url' => route('product.data'),
            'create' => [
                'url' => route('product.create'),
                'label' => 'Tambah Product',
            ],
            'columns' => [
                [
                    'name' => 'name_product',
                    'label' => 'Produk',
                ],
                [
                    'name' => 'category_name',
                    'label' => 'Kategori'
                ],
                [
                    'name' => 'image_product',
                    'label' => 'Gambar',
                ],
                [
                    'name' => 'description',
                    'label' => 'Deskripsi',
                ],
                [
                    'name' => 'price',
                    'label' => 'Harga',
                ],
                [
                    'name' => 'quantity',
                    'label' => 'Kuantiti'
                ],
                [
                    'name' => 'action',
                    'label' => 'Manage'
                ]
            ]
        ];

        return view('admin.products.manage', $data);
    }

    public function getData()
    {
        try {
            $data = Product::select([
                'products.id',
                'products.name_product',
                'products.image_product',
                'products.description',
                'price',
                'quantity',
                'categories.name as category_name'
            ])
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->orderBy('name_product', 'asc');

            return DataTables::of($data)
                ->addColumn('price', function($product) {
                    return 'Rp. '. number_format($product->price, 0, ',', '.');
                })

                ->addColumn('action', function($product) {
                    $string = '<a href="'.route('product.edit', $product->id).'" class="btn btn-info btn-sm mr-3 " style="margin-right: 10px;"><i class="fas fa-edit"></i></a>';

                    $string .= '<button title="Hapus" class="btn btn-icon btn-sm btn-danger waves-effect waves-light delete-form"><i class="fa fa-trash"></i></button>';

                    $string .= '<form action="' . route('product.delete', $product->id) . '" method="POST">' . method_field('delete') . csrf_field() . '</form>';

                    return $string;
                })

                ->rawColumns(['action'])
                ->make(true);
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    public function create()
    {
        $category['categoryList'] = Category::query()->orderBy('name')->get();
        return view('admin.products.form', $category);
    }

    public function store(ProductRequest $request)
    {
        $input = $request->all();

        try {

            if ($request->hasFile('image_product')) {
                $imageFile = $request->image_product;
                $imageProduct = uniqid(). '_' . $imageFile->getClientOriginalExtension();
                $imageFile->move(public_path(). '/images/products/', $imageProduct);
            }

            $input['image_product'] = $imageProduct;
            $input['slug'] = Str::slug($request->input('name_product'));
            $create = Product::create($input);

            if ($create) {
                return redirect()->route('product.manage')->with('success', 'Berhasil menambahkan product');
            }
            return redirect()->back()->with('error', 'Gagal menambahkan data product');

        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function edit(Product $product)
    {
        try {
            $data['product'] = $product;
            $data['categoryList'] = Category::query()->orderBy('name', 'asc')->get();
            return view('admin.products.form', $data);

        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $pathImage = public_path(). '/images/products/'. $product->image_product;

        if ($request->hasFile('image_product')) {
            File::delete($pathImage);
            $imageFile = $request->image_product;
            $imageProduct = uniqid(). '_' . $imageFile->getClientOriginalExtension();
            $imageFile->move(public_path(). '/images/products/', $imageProduct);
        } elseif ($product->image_product) {
            $imageProduct = $product->image_product;
        } else {
            $imageProduct = null;
        }

        $inputUpdate = $request->all();
        $inputUpdate['slug'] = Str::slug($request->input('name_product'));

        try {
            $updateProduct = $product;

            $updateProduct->update($inputUpdate);

            if ($updateProduct->image_product) {
                $inputUpdate['image_product'] = $imageProduct;
            }

            $updateProduct->update($inputUpdate);

            if ($updateProduct) {
                return redirect()->route('product.manage')->with('succes', 'Berhasil mengubah data produk');
            }
            return redirect()->back()->with('error', 'Gagal mengubah data produk');

        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }

    public function delete(String $id)
    {
        $delete = Product::findOrFail($id)->delete();
        try {
            if ($delete) {
                return redirect()->route('product.manage')->with('success', 'Berhasil menghapus data kategori');
            }
            return redirect()->back()->with('error', 'Gagal menghapus data kataegori');
        } catch (\Exception $error) {
            return redirect()->back()->with('error', $error->getMessage());
        }
    }
}
