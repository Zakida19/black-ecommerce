<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required',
            'name_product' => 'required',
            'image_product' => 'nullable|image|mimes:png,jpg,jpeg,gift',
            'description' => 'nullable|string',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
        ];
    }

    public function messages(): array
    {
        return  [
            'category_id.required' => 'Pilih salah satu Kategori produk',
            'name_product.required' => 'Nama produk tidak boleh kosong',
            'image_product.required' => 'image tidak sesuai',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'price.required' => 'Harga produk harus diisi',
            'quantity.required' => 'Jumlah kuantiti produk harus diisi',
        ];
    }
}
