<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware(['auth' => 'admin'])->group(function() {

});

Route::prefix('admin')->group(function() {
    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
});


Route::group(['prefix' => 'category', 'as' => 'category.'], function() {
    Route::get('/manage', [CategoryController::class, 'manage'])->name('manage');
    Route::get('/getdata', [CategoryController::class, 'getData'])->name('data');
    Route::get('/create', [CategoryController::class, 'create'])->name('create');
    Route::post('/create', [CategoryController::class, 'store'])->name('store');
    Route::get('/edit/{category}', [CategoryController::class, 'edit'])->name('edit');
    Route::put('/update/{category}', [CategoryController::class, 'update'])->name('update');
    Route::delete('/delete/{id}', [CategoryController::class, 'delete'])->name('delete');
});

Route::group(['prefix' => 'product', 'as' => 'product.'], function() {
    Route::get('/manage', [ProductController::class, 'manage'])->name('manage');
    Route::get('/getdata', [ProductController::class, 'getData'])->name('data');
    Route::get('/create', [ProductController::class, 'create'])->name('create');
    Route::post('/create', [ProductController::class, 'store'])->name('store');
    Route::get('/edit/{product}', [ProductController::class, 'edit'])->name('edit');
    Route::put('/update/{product}', [ProductController::class, 'update'])->name('update');
    Route::delete('/delete/{product}', [ProductController::class, 'delete'])->name('delete');
});

Route::group(['prefix' => 'orders', 'as' => 'order.'], function() {
    Route::get('/manage', [OrderController::class, 'manage'])->name('manage');
    Route::get('/getdata', [OrderController::class, 'getData'])->name('data');
    Route::get('/detail/{order}', [OrderController::class, 'orderDetail'])->name('detail');
    Route::get('/edit/{order}', [OrderController::class, 'shipped'])->name('edit');
    Route::put('/update/{order}', [OrderController::class, 'update'])->name('update');
    Route::get('/delete/{order}', [OrderController::class, 'delete'])->name('delete');
});
